# v0.9.3 Cleanup
Re-enable mitogen.
push multi-arch docker image
Require version setting for every service

# v1.0 Release Plan

Clean up backup/restore flow
Remove tasks/main.yml if unneeded
