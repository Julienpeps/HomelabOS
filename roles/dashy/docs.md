# Dashy

[Dashy](https://dashy.to/) helps you organize your self-hosted services by making them accessible from a single place.

## Access

It is available at [https://{% if dashy.domain %}{{ dashy.domain }}{% else %}{{ dashy.subdomain + "." + domain }}{% endif %}/](https://{% if dashy.domain %}{{ dashy.domain }}{% else %}{{ dashy.subdomain + "." + domain }}{% endif %}/) or [http://{% if dashy.domain %}{{ dashy.domain }}{% else %}{{ dashy.subdomain + "." + domain }}{% endif %}/](http://{% if dashy.domain %}{{ dashy.domain }}{% else %}{{ dashy.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ dashy.subdomain + "." + tor_domain }}/](http://{{ dashy.subdomain + "." + tor_domain }}/)
{% endif %}